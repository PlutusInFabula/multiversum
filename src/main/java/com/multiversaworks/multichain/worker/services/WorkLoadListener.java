package com.multiversaworks.multichain.worker.services;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.TransactionType;
import com.multiversaworks.multichain.common.model.WalletState;
import com.multiversaworks.multichain.common.services.WalletService;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.TransactionType;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.services.WalletService;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;
import java.time.LocalDateTime;

@Component
public class WorkLoadListener {

    @Autowired
    WalletStateService walletStateService;

    @Autowired
    WalletService walletService;

    @Autowired
    WalletStateManager walletStateManager;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${activemq.queue.consensum-result}")
    String destinationName;

    @JmsListener(destination = "${activemq.topic.work-load}", containerFactory = "myTopicFactory")
    public void receiveTransaction (SimpleTransaction transaction){

        WalletState senderWs  = walletStateService.getLastStateofWallet(transaction.getSenderId());
        boolean vote;

        //  if sender has enough money
        if (senderWs.getActualValue().subtract( transaction.getVariation()).doubleValue() > 0 ){

            //  create a new state with subtracted amount
            WalletState newSenderWalletState = senderWs.clone(transaction.getTransactionID(), TransactionType.SEND, transaction.getVariation(), transaction.getReceiverId());

            // Add new SenderStatus hash to Transaction
            transaction.getLastTransactionsHash().add(newSenderWalletState.getHash());

            //  create a new state with subtracted amount
            WalletState receiverWs  = walletStateService.getLastStateofWallet(transaction.getReceiverId());

            if (receiverWs==null) {
                walletService.unblockWalletId(senderWs.getWalletId());
                return;
            }

            WalletState newReceiverWalletState = receiverWs.clone(transaction.getTransactionID(), TransactionType.RECEIVE, transaction.getVariation(), transaction.getSenderId());

            // put Sender's wallet Status lastHash and id in transaction
            transaction.setSenderStateConnectedHash(senderWs.getHash());
            transaction.setSenderStateConnectedId(senderWs.getStateID());
            // put Receiver's wallet Status lastHash and id in transaction
            transaction.setReceiverStateConnectedHash(receiverWs.getHash());
            transaction.setReceiverStateConnectedId(receiverWs.getStateID());

            // Put New state of sender and receiver in Limbo
            walletStateManager.addToLimboWalletStates(transaction.getTransactionID(), newSenderWalletState, newReceiverWalletState);

            // Add new ReceiverStatus hash to Transaction
            transaction.getLastTransactionsHash().add(newReceiverWalletState.getHash());
            transaction.setHasPositiveConsensum(true);
            // vote is positive
            vote = true;
        } else {
            //  if there isn't enough money: vore is negative
            vote = false;
        }

        //  send back consensum
        TransactionVote finalVote = buildTransactionVote(transaction, vote);
        jmsTemplate.convertAndSend(destinationName, finalVote);
    }

    private TransactionVote buildTransactionVote(SimpleTransaction transaction, boolean vote) {
        TransactionVote finalVote = new TransactionVote();
        finalVote.setVoterId("This Node Id");
        finalVote.setTransaction(transaction);
        finalVote.setVotingTime(LocalDateTime.now());
        finalVote.setTransactionId(transaction.getTransactionID());
        finalVote.setHasPositiveConsensum(vote);
        return finalVote;
    }


}
