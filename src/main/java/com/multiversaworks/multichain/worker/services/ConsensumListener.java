package com.multiversaworks.multichain.worker.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

@Component
public class ConsensumListener {

    @Autowired
    WalletStateManager walletStateManager;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${activemq.queue.consensum-result}")
    String destinationName;

    @JmsListener(destination = "${activemq.topic.finalizedTransaction}", containerFactory = "myTopicFactory")
    public void receiveTransaction (TransactionVote vote){
       walletStateManager.persistConnectedWalletStates(vote.getTransactionId());
    }



}
