package com.multiversaworks.multichain.worker.services;

import com.multiversaworks.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Data
@Component @Scope("singleton")
public class WalletStateManager {

    @Autowired
    WalletStateRepo repo;

    private HashMap<String, List<WalletState>> walletStateLimbo = new HashMap<>();

    public void persistConnectedWalletStates(String transactionId){
        List<WalletState> walletStates = walletStateLimbo.get(transactionId);
        walletStates.forEach(walletState -> repo.save(walletState));
        walletStateLimbo.remove(transactionId);
    }

    public void addToLimboWalletStates(String transactionId, WalletState senderWalletState, WalletState receiverWalletState){
        ArrayList<WalletState> walletStates = new ArrayList<>();
        walletStates.add(senderWalletState);
        walletStates.add(receiverWalletState);
        walletStateLimbo.put(transactionId, walletStates);
    }

    public void sendToHellWalletStates(String transactionId){
        walletStateLimbo.remove(transactionId);
    }

}
