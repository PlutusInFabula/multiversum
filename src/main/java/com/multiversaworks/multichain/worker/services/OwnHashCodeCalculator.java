package com.multiversaworks.multichain.worker.services;

import com.universaworks.multiversa.multichain.server.rest.StartTransaction;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class OwnHashCodeCalculator {

    public String doIt(){

    File file = new java.io.File(StartTransaction.class.getProtectionDomain()
            .getCodeSource()
            .getLocation()
            .getPath());

        MessageDigest md = null;
        try {
            md = MessageDigest.getInstance("MD5");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        try (InputStream is = new FileInputStream(file);
             DigestInputStream dis = new DigestInputStream(is, md))
        {
            /* Read decorated stream (dis) to EOF as normal... */
        } catch (IOException e) {
            e.printStackTrace();
        }
        byte[] digest = md.digest();
        String result = digest.toString();
    System.out.println("result: " + result);
        return result;
    }


}
