package com.multiversaworks.multichain.worker.services;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import lombok.Data;
import java.time.LocalDateTime;

@Data
public class TransactionVote {
    private String voterId;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    private LocalDateTime votingTime;
    private String TransactionId;
    private SimpleTransaction transaction;
    private boolean hasPositiveConsensum;
}
