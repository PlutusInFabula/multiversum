package com.multiversaworks.multichain.worker.services;

import com.multiversaworks.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import lombok.Data;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Data
@Component
@Scope("singleton")
public class WalletManager {

    private ArrayList<Wallet> wallets;

    @Cacheable("wallets")
    public ArrayList<Wallet> getWallets(){
        return wallets;
    }
    @CachePut("wallets")
    public Wallet addWallet(Wallet wallet){
        this.wallets.add(wallet);
        return wallet;
    }


}
