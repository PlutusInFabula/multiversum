package com.multiversaworks.multichain.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.Data;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
@JsonIgnoreProperties(ignoreUnknown = true)
public class WalletState implements Chainable{

    @Id
    private String stateID = UUID.randomUUID().toString();
    @Column
    private String walletId;
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column
    private LocalDateTime timestamp = LocalDateTime.now();
    @Column
    private BigDecimal actualValue;
    @Column
    private BigDecimal variation;
    @Column
    private String otherPart;
    @Column
    private TransactionType transactionType;
    @Column
    private String hash;
    @Column
    private String previousHash;
    @Column
    private String originTransactionId;

    @Override
    @Transient
    public String calculateOwnHash() {
        StringBuilder sb = new StringBuilder();
        sb      .append(stateID)
                .append(walletId)
                .append(timestamp)
                .append(actualValue)
                .append(variation)
                .append(otherPart)
                .append(transactionType)
                .append(hash)
                .append(previousHash)
                .append(originTransactionId);

        String result = DigestUtils.sha256Hex(sb.toString());
        return result;
    }

    @Transient
    public WalletState clone(String originTransactionId, TransactionType type, BigDecimal value, String otherPart){
        WalletState result = new WalletState();
        result.setWalletId(this.walletId);
        result.setVariation(value);
        result.setOtherPart(otherPart);
        result.setTransactionType(type);
        result.previousHash = this.hash;
        result.setOriginTransactionId(originTransactionId);
        if(type==TransactionType.SEND){
            result.actualValue=this.actualValue.subtract(value);
        } else if (type==TransactionType.RECEIVE){
            result.actualValue=this.actualValue.add(value);
        }
        result.setHash(result.calculateOwnHash());
        return result;
    }
}
