package com.multiversaworks.multichain.common.model;

public enum TransactionType {
    SEND, RECEIVE
}
