package com.multiversaworks.multichain.common.model;

public class PageRequestWrapper {
    private int page;
    private int lenght;

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getLenght() {
        return lenght;
    }

    public void setLenght(int lenght) {
        this.lenght = lenght;
    }
}
