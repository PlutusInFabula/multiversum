package com.multiversaworks.multichain.common.model;

import lombok.Data;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class User {

    @Id
    private String ID = UUID.randomUUID().toString();
    private String name;
    private String password;
    private String email;
    private String token;
    @ElementCollection(targetClass=String.class, fetch = FetchType.EAGER)
    private List<String> wallets = new ArrayList<>();

}
