package com.multiversaworks.multichain.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleTransactionScheletonDTO {

    private String transactionID ; //= UUID.randomUUID().toString();
    private String transactionHash; // Calculated on Coordinators after consensum
    private ArrayList<String> lastTransactionsHash; // listed on Coordinators after consensum
    private boolean hasPositiveConsensum;

}
