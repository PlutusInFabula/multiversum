package com.multiversaworks.multichain.common.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

public interface MainChain extends Chainable, Serializable {
    String getTransactionID();

    void setTransactionID(String transactionID);

    String getTransactionHash();

    void setTransactionHash(String transactionHash);

    List<String> getLastTransactionsHash();

    void setLastTransactionsHash(List<String> lastTransactionsHash);

    LocalDateTime getTransactionStart();

    void setTransactionStart(LocalDateTime transactionStart);

    LocalDateTime getTransactionFinished();

    void setTransactionFinished(LocalDateTime transactionFinished);

    boolean isHasPositiveConsensum();

    void setHasPositiveConsensum(boolean hasPositiveConsensum);

    byte[] getSenderSignature();

    void setSenderSignature(byte[] senderSignature);
}
