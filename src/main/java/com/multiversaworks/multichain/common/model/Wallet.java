package com.multiversaworks.multichain.common.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Entity
public class Wallet {

    @Id
    /*@GeneratedValue(generator = "system-uuid")
    @GenericGenerator(name = "system-uuid", strategy = "uuid")*/
    private String walletId;
    @Column(length = 128)
    private byte[] public_key;
    @Column(length = 512)
    private byte[] private_key;

}
