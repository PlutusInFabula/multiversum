package com.multiversaworks.multichain.common.model;

public interface Erc20 {

    int totalSupply();
    int balanceOf(String walletId);
    boolean transfer(String receiverWalletId, int value);
    boolean transferFrom(String senderWalledId, String receiverWalletId, int value);
    boolean approve(String spenderWalledId, int _value);
    int allowance(String walletId, String spenderWalledId);
    boolean Transfer(String senderWalledId, String receiverWalletId, int value);
    boolean Approval(String walletId, String spenderWalledId, int _value);

}
