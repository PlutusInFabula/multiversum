package com.multiversaworks.multichain.common.model;

import java.io.Serializable;

public interface Chainable extends Serializable, Cloneable {

    String calculateOwnHash();

}
