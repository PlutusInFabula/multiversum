package com.multiversaworks.multichain.common.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import lombok.NoArgsConstructor;
import org.apache.commons.codec.digest.DigestUtils;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.UUID;

@Entity
@NoArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class SimpleTransaction implements MainChain {

    @Id
    private String transactionID = UUID.randomUUID().toString();                  // calculated by Client;

    @Column
    private String senderId;                        // SENT BY SENDER
    @Column
    private String receiverId;                      // SENT BY SENDER
    @Column(length = 128)
    private byte[] senderSignature;                 // SENT BY SENDER
    @Column
    private BigDecimal variation;                   // SENT BY SENDER
    @Column
    private String text;                            // SENT BY SENDER

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column
    private LocalDateTime transactionStart;          // Calculated by Client

    @Column
    private String senderStateConnectedId;        // calculated by workers
    @Column
    private String receiverStateConnectedId;      // calculated by workers

    @Column
    private String senderStateConnectedHash;        // calculated by workers
    @Column
    private String receiverStateConnectedHash;      // calculated by workers

    @Column
    private boolean hasPositiveConsensum;
    @ElementCollection(targetClass=String.class)
    private List<String> lastTransactionsHash = new ArrayList<>(); // listed on Coordinators after consensum

    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
    @Column
    private LocalDateTime transactionFinished;

    @Column
    private String transactionHash;                 // Calculated on Coordinators after consensum at the real end

    public SimpleTransaction(String senderId, String receiverId, BigDecimal variation, String text, byte[] senderSignature) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.variation = variation;
        this.text = text;
        this.senderSignature = senderSignature;
    }

    @Override
    @Transient
    public String calculateOwnHash() {
        StringBuilder sb = new StringBuilder();
        sb.append(transactionID)
                .append(senderId)
                .append(receiverId)
                .append(senderSignature)
                .append(variation)
                .append(text)
                .append(transactionStart)
                .append(senderStateConnectedId)
                .append(receiverStateConnectedId)
                .append(senderStateConnectedHash)
                .append(receiverStateConnectedHash)
                .append(hasPositiveConsensum);

        // It's necessary to have the same order of previous hashes to calculate the same hash for the transaction!
        lastTransactionsHash.sort(Comparator.naturalOrder());
        lastTransactionsHash.forEach(sb::append);
        //lastTransactionsHash.forEach(item -> sb.append(item));
        sb.append(transactionFinished);

        String result = DigestUtils.sha1Hex(sb.toString());
        return result;
    }

    @Override
    public String getTransactionID() {
        return transactionID;
    }

    @Override
    public void setTransactionID(String transactionID) {
        this.transactionID = transactionID;
    }

    @Override
    public String getTransactionHash() {
        return transactionHash;
    }

    @Override
    public void setTransactionHash(String transactionHash) {
        this.transactionHash = transactionHash;
    }

    @Override
    public List<String> getLastTransactionsHash() {
        return lastTransactionsHash;
    }

    @Override
    public void setLastTransactionsHash(List<String> lastTransactionsHash) {
        this.lastTransactionsHash = lastTransactionsHash;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderStateConnectedId() {
        return senderStateConnectedId;
    }

    public void setSenderStateConnectedId(String senderStateConnectedId) {
        this.senderStateConnectedId = senderStateConnectedId;
    }

    public String getReceiverStateConnectedId() {
        return receiverStateConnectedId;
    }

    public void setReceiverStateConnectedId(String receiverStateConnectedId) {
        this.receiverStateConnectedId = receiverStateConnectedId;
    }

    public String getSenderStateConnectedHash() {
        return senderStateConnectedHash;
    }

    public void setSenderStateConnectedHash(String senderStateConnectedHash) {
        this.senderStateConnectedHash = senderStateConnectedHash;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverStateConnectedHash() {
        return receiverStateConnectedHash;
    }

    public void setReceiverStateConnectedHash(String receiverStateConnectedHash) {
        this.receiverStateConnectedHash = receiverStateConnectedHash;
    }

    public BigDecimal getVariation() {
        return variation;
    }

    public void setVariation(BigDecimal variation) {
        this.variation = variation;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public LocalDateTime getTransactionStart() {
        return transactionStart;
    }

    @Override
    public void setTransactionStart(LocalDateTime transactionStart) {
        this.transactionStart = transactionStart;
    }

    @Override
    public LocalDateTime getTransactionFinished() {
        return transactionFinished;
    }

    @Override
    public void setTransactionFinished(LocalDateTime transactionFinished) {
        this.transactionFinished = transactionFinished;
    }

    @Override
    public boolean isHasPositiveConsensum() {
        return hasPositiveConsensum;
    }

    @Override
    public void setHasPositiveConsensum(boolean hasPositiveConsensum) {
        this.hasPositiveConsensum = hasPositiveConsensum;
    }

    @Override
    public byte[] getSenderSignature() {
        return senderSignature;
    }

    @Override
    public void setSenderSignature(byte[] senderSignature) {
        this.senderSignature = senderSignature;
    }
}
