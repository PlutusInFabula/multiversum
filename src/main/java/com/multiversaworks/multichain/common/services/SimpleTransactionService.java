package com.multiversaworks.multichain.common.services;

import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public interface SimpleTransactionService {

    ArrayList<String> getBlockedHashes();

    boolean isBlocked(String hastToCheck);

    boolean blockHash(String hashToBlock);

    boolean unblockHash(String hashToUnblock);

    ArrayList<String> getBurntHashes();

    boolean isBurnt(String hastToCheck);

    boolean burnHash(String hashToBurn);

    LinkedList<String> getRunningTransactions();

    boolean isTransactionRunnung(String transactionId);

    boolean addRunningTransaction(String transactionId);

    boolean removeRunningTransaction(String transactionId);

    String getLastTransactionId();

    String getLastTransactionHash();

    void setLastTransactionId(String lastTransactionId);

    void setLastTransactionHash(String lastTransactionHash);

    List<SimpleTransaction> getTransactionsForWalletId(String walletId);
}
