package com.multiversaworks.multichain.common.services;

import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Data
@Service
@Scope("singleton")
public class WalletServiceImplemented implements WalletService {

    @Autowired
    WalletRepo walletRepo;

    private String lastTransactionId;
    private String lastTransactionHash;

    private ArrayList<String> blockedWalletIds = new ArrayList<>();

    @Override
    @Cacheable("blockedWalletIds")
    public ArrayList<String> getBlockedWalletIds() {
        return blockedWalletIds;
    }

    @Override
    public boolean isBlocked(String walletIdToCheck){
        return this.getBlockedWalletIds().contains(walletIdToCheck);
    }

    @Override
    @CachePut("blockedWalletIds")
    public boolean blockWallet(String walletIdToBlock){
        blockedWalletIds.add(walletIdToBlock);
        return true;
    }

    @Override
    @CacheEvict(value = "blockedWalletIds", allEntries = false)
    public boolean unblockWalletId(String hashToUnblock){
        return blockedWalletIds.remove(hashToUnblock);
    }

    @Override
    public Wallet getWalletById(String walletId) {
        return walletRepo.findOneByWalletId(walletId);
    }

}
