package com.multiversaworks.multichain.common.services;

import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.repositories.SimpleTransactionRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Data
@Service
@Scope("singleton")
public class SimpleTransactionServiceImplemented implements SimpleTransactionService {

    @Autowired
    SimpleTransactionRepo simpleTransactionRepo;

    // ToDo: when net is existing this id shall be equal to the last created and persisted in Database
    private String lastTransactionId = "0000000000000000-firstTransactionId";
    // ToDo: when net is existing this hash shall be equal to the last created and persisted in Database
    private String lastTransactionHash= "0000000000000000-firstHash";
    private LinkedList<String> runningTransactions = new LinkedList<>();

    private ArrayList<String> blockedHash = new ArrayList<>();
    private ArrayList<String> burntHashes = new ArrayList<>();

    @Override
    @Cacheable("blockedTransactions")
    public ArrayList<String> getBlockedHashes() {
        return blockedHash;
    }

    @Override
    public boolean isBlocked(String hastToCheck){
        return this.getBlockedHash().contains(hastToCheck);
    }

    @Override
    @CachePut("blockedTransactions")
    public boolean blockHash(String hashToBlock){
        blockedHash.add(hashToBlock);
        return true;
    }

    @Override
    @CacheEvict(value = "blockedTransactions", allEntries = false)
    public boolean unblockHash(String hashToUnblock){
        return blockedHash.remove(hashToUnblock);
    }

    @Override
    @Cacheable("runningTransactions")
    public LinkedList<String> getRunningTransactions(){
        return runningTransactions;
    }

    @Override
    public boolean isTransactionRunnung(String transactionId){
        return this.getRunningTransactions().contains(transactionId);
    }

    @Override
    @CachePut("runningTransactions")
    public boolean addRunningTransaction(String transactionId){
        return runningTransactions.add(transactionId);
    }

    @Override
    @CacheEvict(value = "runningTransactions", allEntries = false)
    public boolean removeRunningTransaction(String transactionId){
        return runningTransactions.remove(transactionId);

    }

    @Override
    @Cacheable("burntTransactions")
    public ArrayList<String> getBurntHashes(){
        return this.burntHashes;
    }

    @Override
    public boolean isBurnt(String hastToCheck) {
        return this.getBurntHashes().contains(hastToCheck);
    }

    @Override
    @CachePut("burntTransactions")
    public boolean burnHash(String hashToBurn) {
        return this.burntHashes.add(hashToBurn);
    }

    public String getLastTransactionHash(){
        return this.lastTransactionHash;
    }

    @Override
    public List<SimpleTransaction> getTransactionsForWalletId(String walletId) {
        return simpleTransactionRepo.findBySenderIdOrReceiverIdOrderByTransactionFinished(walletId, walletId);
    }

}
