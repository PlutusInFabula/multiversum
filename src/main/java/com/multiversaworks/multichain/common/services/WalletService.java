package com.multiversaworks.multichain.common.services;

import com.multiversaworks.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;

public interface WalletService {

    public ArrayList<String> getBlockedWalletIds();

    boolean isBlocked(String walletIdToCheck);

    boolean blockWallet(String walletToBlock);

    boolean unblockWalletId(String WalletIdToUnblock);

    String getLastTransactionId();

    String getLastTransactionHash();

    void setLastTransactionId(String lastTransactionId);

    void setLastTransactionHash(String lastTransactionHash);

    Wallet getWalletById(String walletId);
}
