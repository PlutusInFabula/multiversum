package com.multiversaworks.multichain.common.services;

import com.universaworks.multiversa.multichain.common.model.WalletState;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;

import java.util.ArrayList;

public interface WalletStateService {
    boolean isBlocked(String hastToCheck);

    boolean blockWalletStateId(String WalletStateIdToBlock);

    boolean unblockWalletStateId(String WalletStateIdToUnblock);

    ArrayList<String> getBurntWalletStateIds();

    boolean isBurnt(String WalletStateIdToCheck);

    boolean burnWalletStateId(String WalletStateIdToBurn);

    String getLastTransactionId();

    String getLastTransactionHash();

    void setLastTransactionId(String lastTransactionId);

    void setLastTransactionHash(String lastTransactionHash);

    WalletState getLastStateofWallet(String walletId);
}
