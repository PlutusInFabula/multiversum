package com.multiversaworks.multichain.common.services;

import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Data
@Service
@Scope("singleton")
public class WalletStateServiceImplemented implements WalletStateService {

    @Autowired
    WalletStateRepo walletStateRepo;

    private String lastTransactionId;
    private String lastTransactionHash;

    private ArrayList<String> blockedHashes = new ArrayList<>();
    private ArrayList<String> burntHashes = new ArrayList<>();

    @Cacheable("blockedWalletStateIds")
    public ArrayList<String> getBlockedHashes() {
        return blockedHashes;
    }

    @Override
    public boolean isBlocked(String hastToCheck){
        return this.getBlockedHashes().contains(hastToCheck);
    }

    @Override
    @CachePut("blockedWalletStateIds")
    public boolean blockWalletStateId(String hashToBlock){
        blockedHashes.add(hashToBlock);
        return true;
    }

    @Override
    @CacheEvict(value = "blockedWalletStateIds", allEntries = false)
    public boolean unblockWalletStateId(String hashToUnblock){
        return blockedHashes.remove(hashToUnblock);
    }

    @Cacheable("burntWalletStateIds")
    public ArrayList<String> getBurntWalletStateIds(){
        return this.burntHashes;
    }

    @Override
    public boolean isBurnt(String hastToCheck) {
        return this.getBurntWalletStateIds().contains(hastToCheck);
    }

    @Override
    @CachePut("burntWalletStateIds")
    public boolean burnWalletStateId(String hashToBurn) {
        return this.burntHashes.add(hashToBurn);
    }

    @Override
    public WalletState getLastStateofWallet(String walletId) {
        return walletStateRepo.findFirstByWalletIdOrderByTimestampDesc(walletId);
    }


}
