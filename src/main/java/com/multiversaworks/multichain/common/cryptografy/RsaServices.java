package com.multiversaworks.multichain.common.cryptografy;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.crypto.Cipher;
import javax.crypto.NoSuchPaddingException;
import java.security.*;
import java.security.spec.PKCS8EncodedKeySpec;
import java.security.spec.X509EncodedKeySpec;


@Service
public class RsaServices {

    KeyPairGenerator keyGen;
    KeyFactory keyFactory ;
    Cipher cipher;

    private static final String ALGORITHM = "RSA";

    public  byte[] encrypt(byte[] publicKey, byte[] inputData) throws Exception {

        PublicKey key = keyFactory.generatePublic(new X509EncodedKeySpec(publicKey));

        cipher.init(Cipher.PUBLIC_KEY, key);

        byte[] encryptedBytes = cipher.doFinal(inputData);
        return encryptedBytes;
    }

    public  String  decrypt(byte[] privateKey, byte[] inputData) throws Exception {

        PrivateKey key = keyFactory.generatePrivate(new PKCS8EncodedKeySpec(privateKey));

        cipher.init(Cipher.PRIVATE_KEY, key);

        byte[] decryptedBytes = cipher.doFinal(inputData);
        return new String (decryptedBytes);
    }

    public KeyPair generateKeyPair()  {

        SecureRandom random = null;
        try {
            random = SecureRandom.getInstance("SHA1PRNG", "SUN");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }

        // 512 is keysize
        keyGen.initialize(512, random);

        KeyPair generateKeyPair = keyGen.generateKeyPair();
        return generateKeyPair;
    }
@PostConstruct
    public void init() throws Exception {

    keyGen = KeyPairGenerator.getInstance(ALGORITHM);
    keyFactory = KeyFactory.getInstance(ALGORITHM);
    cipher = Cipher.getInstance(ALGORITHM);

    }


}
