package com.multiversaworks.multichain.common.repositories;

import com.multiversaworks.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface WalletRepo extends JpaRepository<Wallet, String> {

    Wallet findOneByWalletId(String walletId);

    Wallet findFirstByWalletId(String walletId);

    List<Wallet> findByWalletIdIn(List<String> wallets);

    Wallet findFirstByWalletIdIn(List list);

    @Query("SELECT COUNT(w) FROM Wallet w")
    Long countAll();
}
