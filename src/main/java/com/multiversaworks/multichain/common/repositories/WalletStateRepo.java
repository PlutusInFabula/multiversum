package com.multiversaworks.multichain.common.repositories;

import com.multiversaworks.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface WalletStateRepo extends JpaRepository<WalletState, String> {

    //@Query("SELECT ws from WalletState ws where ws.walletId = (:walletId) ORDER BY ws.timestamp DESC  LIMIT 1")
    //public WalletState getLastStateofWallet(@Param("walletId") String walletId);

public WalletState findFirstByWalletIdOrderByTimestampDesc(String walletId);
}
