package com.multiversaworks.multichain.common.repositories;

import com.multiversaworks.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface UserRepo extends JpaRepository<User, String> {

    User findOneByEmailAndPassword(String email, String password);
    User findOneByEmail(String email);
    User findFirstByWalletsContains(String walletId);


}
