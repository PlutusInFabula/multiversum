package com.multiversaworks.multichain.common.repositories;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.List;


public interface SimpleTransactionRepo extends JpaRepository<SimpleTransaction, String> {


    List<SimpleTransaction> findBySenderIdOrReceiverIdOrderByTransactionFinished(String senderId, String receiverId);
}
