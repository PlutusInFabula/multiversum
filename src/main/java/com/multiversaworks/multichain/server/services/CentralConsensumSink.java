package com.multiversaworks.multichain.server.services;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.User;
import com.multiversaworks.multichain.common.services.SimpleTransactionService;
import com.multiversaworks.multichain.common.services.WalletService;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.multiversaworks.multichain.worker.services.TransactionVote;
import com.multiversaworks.multichain.worker.services.WalletStateManager;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.repositories.SimpleTransactionRepo;
import com.universaworks.multiversa.multichain.common.repositories.UserRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import com.universaworks.multiversa.multichain.common.services.SimpleTransactionService;
import com.universaworks.multiversa.multichain.common.services.WalletService;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.worker.services.TransactionVote;
import com.universaworks.multiversa.multichain.worker.services.WalletStateManager;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Scope;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@Data
@Component
@Scope("singleton")
public class CentralConsensumSink {

    @Autowired
    CentralFreezingManager hashManager;

    @Autowired
    JmsTemplate jmsTemplate;

    @Autowired
    WalletStateManager walletStateManager;

    @Autowired
    SimpleTransactionRepo simpleTransactionRepo;

    @Autowired
    SimpleTransactionService simpleTransactionService;

    @Autowired
    WalletStateService walletStateService;

    @Autowired
    WalletRepo walletRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    WalletService walletService;

    @Autowired
    MessagePusher messagePusher;

    @Value("${activemq.topic.finalizedTransaction}")
    String consensumResultDestination;

    @Value("${totalRunningWorkExecutor}")
    int totalRunningWorkExecutor;
    int minimalfraction = totalRunningWorkExecutor / 2;

    HashMap <SimpleTransaction, ArrayList<TransactionVote>> voteResults = new HashMap<>();

    // cached listener for work executors results'es
    @JmsListener(destination = "${activemq.queue.consensum-result}", containerFactory = "myQueueFactory")
    public void receiveTransaction(TransactionVote vote){

        SimpleTransaction actualTransaction = null;

        ArrayList<TransactionVote> listOfRelevantVotes;
        //  add vote to map or refresh voteResults list.
        if (voteResults.containsKey(vote.getTransaction())){
            listOfRelevantVotes = voteResults.get(vote.getTransaction());
            listOfRelevantVotes.add(vote);
        } else {
            listOfRelevantVotes = new ArrayList<>();
            listOfRelevantVotes.add(vote);
            voteResults.put(vote.getTransaction(), listOfRelevantVotes);
        }

        if (listOfRelevantVotes.size() > minimalfraction){
            if ( listOfRelevantVotes.stream().filter(v -> v.isHasPositiveConsensum()).count() > minimalfraction){

                // ToDo: Added last TransactionHash: verify when is necessary to add more
                 actualTransaction = vote.getTransaction();

                actualTransaction.setLastTransactionsHash(new ArrayList<String>(){{add(hashManager.getSimpleTransactionService().getLastTransactionHash());}});
                actualTransaction.setTransactionFinished(LocalDateTime.now());
                actualTransaction.setTransactionHash(actualTransaction.calculateOwnHash());

                simpleTransactionRepo.save(actualTransaction);

                // add last TransactionHash and Id to TransactionService
                simpleTransactionService.setLastTransactionHash(actualTransaction.getTransactionHash());
                simpleTransactionService.setLastTransactionId(actualTransaction.getTransactionID());

                // burn old hashes
                walletStateService.burnWalletStateId(actualTransaction.getSenderStateConnectedId());
                walletStateService.burnWalletStateId(actualTransaction.getReceiverStateConnectedId());

                // reove transaction from voteResults
                simpleTransactionService.removeRunningTransaction(vote.getTransactionId());


                // PUBLISH new states of wallets
                jmsTemplate.convertAndSend(consensumResultDestination, vote );
            } else if (listOfRelevantVotes.stream().filter(v -> !v.isHasPositiveConsensum()).count() > minimalfraction) {
                walletStateManager.sendToHellWalletStates(vote.getTransactionId());
            }

            // and unblock hashes,
            walletService.unblockWalletId(vote.getTransaction().getSenderId());
            //walletService.unblockWalletId(vote.getTransaction().getReceiverId());

            if (actualTransaction != null && actualTransaction.getSenderId().equals("MasterWallet")){

               // String receiverWalletId = walletRepo.findOneByWalletId(actualTransaction.getReceiverId()).getWalletId();
                String receiverToken = null;
                List<User> receivers = userRepo.findAll();
                for (User u : receivers){
                    if (u.getWallets().contains(actualTransaction.getReceiverId())) {
                        receiverToken = u.getToken();
                        break;
                    }
                }
                String senderName = null;

                //String senderWalletId = walletRepo.findOneByWalletId(actualTransaction.getReceiverId()).getWalletId();
                List<User> senders = userRepo.findAll();
                for (User u : senders){
                    if (u.getWallets().contains(actualTransaction.getSenderId())) {
                        senderName = u.getName();
                        break;
                    }
                }

                      //  String senderId = walletRepo.findOneByWalletId(actualTransaction.getReceiverId()).getWalletId();
                //String receiverToken = userRepo.findFirstByWalletsContains(receiverWalletId).getToken();
                //String senderName = userRepo.findFirstByWalletsContains(senderId).getName();
                if (receiverToken!= null) messagePusher.pushMessage(senderName, receiverToken, actualTransaction.getVariation());
            }

        }
    }

}
