package com.multiversaworks.multichain.server.services;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.LinkedList;

@Service
public class ParallelTransactionProcessor {

    @Autowired
    CentralFreezingManager cache;

    @Autowired
    WalletStateRepo walletStateRepo;

    @Autowired
    private JmsTemplate jmsTemplate;

    @Value("${activemq.topic.work-load}")
    String destinationName;

    LinkedList<SimpleTransaction> hangedTransactions = new LinkedList<>();


    public void worker(){
        while(true){

            SimpleTransaction actualTransaction = hangedTransactions.removeFirst();

            if (cache.getWalletService().isBlocked(actualTransaction.getSenderId()) || cache.getWalletService().isBlocked(actualTransaction.getReceiverId()) ) {
                this.addToQueue(actualTransaction);
            }

        }
    }

    public ResponseEntity<String> processTransactionWuthUnblockedActors( SimpleTransaction transaction) {
        //   block transaction actors hashes

        cache.getWalletService().blockWallet(transaction.getSenderId());
        //cache.getWalletService().blockWallet(transaction.getReceiverId());

        // ToDo: receiver can receive multiple transaction


        //  add actual transaction in running transactions
        cache.getSimpleTransactionService().addRunningTransaction(transaction.getTransactionID());

        //  publish transaction
        jmsTemplate.convertAndSend(destinationName, transaction);

        return new ResponseEntity<>("message received. transaction started", HttpStatus.ACCEPTED);
    }

    public void addToQueue(SimpleTransaction transaction) {
        this.hangedTransactions.add(transaction);
    }
}
