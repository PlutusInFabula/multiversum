package com.multiversaworks.multichain.server.services;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;

@Component
public class MessagePusher {
    @Value("${androidFcmKey}")
    String androidFcmKey;

    public void pushMessage(String senderName, String receiverToken, BigDecimal variation){
        try {

            String androidFcmUrl="https://fcm.googleapis.com/fcm/send";

            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders httpHeaders = new HttpHeaders();
            httpHeaders.set("Authorization", "key=" + androidFcmKey);
            httpHeaders.set("Content-Type", "application/json");
            JSONObject msg = new JSONObject();
            JSONObject json = new JSONObject();

            msg.put("title", "Great news!!!");
            msg.put("body", "Received " + variation + " Enki from " + senderName + "!");
            msg.put("notificationType", "Test");

            json.put("data", msg);
            json.put("to", receiverToken);

            HttpEntity<String> httpEntity = new HttpEntity<String>(json.toString(),httpHeaders);
            restTemplate.postForObject(androidFcmUrl,httpEntity,String.class);

        } catch (Exception e) {
            System.out.println("Impossible to push notification due to exception of type: " + e.getClass());
        }
    }
}
