package com.multiversaworks.multichain.server.services;

import com.multiversaworks.multichain.common.services.SimpleTransactionService;
import com.multiversaworks.multichain.common.services.WalletService;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.common.services.SimpleTransactionService;
import com.universaworks.multiversa.multichain.common.services.WalletService;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Data
@Component
@Scope("singleton")
public class CentralFreezingManager {

    @Autowired
    SimpleTransactionService simpleTransactionService;
    @Autowired
    WalletService walletService;
    @Autowired
    WalletStateService walletStateService;

}
