package com.multiversaworks.multichain.server.rest;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.server.services.CentralFreezingManager;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import com.universaworks.multiversa.multichain.server.services.CentralFreezingManager;
import com.universaworks.multiversa.multichain.server.services.ParallelTransactionProcessor;
import org.apache.activemq.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;

@RestController
public class StartTransaction {

    @Autowired
    CentralFreezingManager cache;

    @Autowired
    WalletStateRepo walletStateRepo;

    @Autowired
    private ParallelTransactionProcessor parallelTransactionProcessor;



    @RequestMapping(value = "/start-transaction", method = RequestMethod.POST)
    public ResponseEntity startTransaction(@RequestBody SimpleTransaction transaction){

        transaction.setTransactionStart(LocalDateTime.now());
        
        // checking whether transaction actors's wallet (or WalletState?) hashes are blocked
        if (cache.getWalletService().isBlocked(transaction.getSenderId()) || cache.getWalletService().isBlocked(transaction.getReceiverId()) ) {
            putTransactionInQueue(transaction);
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Sender account is blocked. try again.");
        }

        ResponseEntity<String> response = parallelTransactionProcessor.processTransactionWuthUnblockedActors(transaction);

        return response;
    }

    private void putTransactionInQueue(SimpleTransaction transaction) {
        parallelTransactionProcessor.addToQueue(transaction);
    }



}
