package com.multiversaworks.multichain.client.controllers.MVC;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.client.services.RestClient;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

public class WalletMvcController {

    @Autowired
    RestClient client;

    Wallet wallet ;         // The wallet of the customer
    WalletState lastState ; // The last available state of the Wallet
    // TODO: add useful methods to see past transaction, QRCode to receive, send transaction, actual values, state of confirmations

    public String startTransaction(Model model){

        SimpleTransaction transaction = new SimpleTransaction();

        client.postRequest(transaction );
        return "WaitForResult";
    }
}
