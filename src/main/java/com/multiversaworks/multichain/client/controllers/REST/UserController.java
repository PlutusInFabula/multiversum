package com.multiversaworks.multichain.client.controllers.REST;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.User;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.client.services.RestClient;
import com.universaworks.multiversa.multichain.common.cryptografy.RsaServices;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.UserRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.security.KeyPair;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@RestController
public class UserController {

    @Autowired
    UserRepo userRepo;
    @Autowired
    WalletRepo walletRepo;
    @Autowired
    WalletStateRepo walletStateRepo;
    @Autowired
    RestClient client;

    @Autowired
    RsaServices rsaServices;

    @RequestMapping(value = "/registerUser", method = RequestMethod.POST)
    public ResponseEntity registerUser (@RequestBody User newUser){

        //ToDO: make it distributed!

        if (  StringUtils.isEmpty(newUser.getName())
               || StringUtils.isEmpty(newUser.getPassword())
               || StringUtils.isEmpty(newUser.getEmail())
               // || StringUtils.isEmpty((newUser.getToken()))
                ) {
            return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
        }

        if ( userRepo.findOneByEmail(newUser.getEmail()) != null){
            return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Email already registered");
        }

        Wallet newWallet = new Wallet();
        newWallet.setWalletId(UUID.randomUUID().toString());
        newUser.getWallets().add(newWallet.getWalletId());

        KeyPair keyPair = rsaServices.generateKeyPair();
        newWallet.setPrivate_key(keyPair.getPrivate().getEncoded());
        newWallet.setPublic_key(keyPair.getPublic().getEncoded());

        WalletState newWalletState = new WalletState();
        newWalletState.setWalletId(newWallet.getWalletId());
        newWalletState.setActualValue(BigDecimal.valueOf(0));
        // Check: zero hash or last Transaction?
        newWalletState.setPreviousHash("zero_hash");
        newWalletState.setTimestamp(LocalDateTime.now().minus(1, ChronoUnit.DAYS));
        newWalletState.setHash(newWalletState.calculateOwnHash());

        userRepo.save(newUser);
        walletRepo.save(newWallet);
        ArrayList <Wallet> walletList = new ArrayList<>();
        walletList.add(newWallet);
        walletStateRepo.save(newWalletState);

        SimpleTransaction masterTransaction = generateMasterTransaction(newWallet.getWalletId(), BigDecimal.valueOf(10000));

        client.postRequest(masterTransaction);

        return new ResponseEntity<>(new UserRestWrapper(newUser, walletList), HttpStatus.OK);

    }

    private SimpleTransaction generateMasterTransaction(String receiverId, BigDecimal variation) {
        SimpleTransaction result = new SimpleTransaction();
        result.setSenderId("MasterWallet");
        result.setReceiverId(receiverId);
        result.setVariation(variation);
        result.setTransactionStart(LocalDateTime.now());
        StringBuilder sb = new StringBuilder();
        sb.append(result.getSenderId()).append(result.getReceiverId()).append(result.getVariation()).append(result.getTransactionStart());

        String dataHash = DigestUtils.sha1Hex(sb.toString());
        Wallet masterWallet = walletRepo.findOneByWalletId("MasterWallet");
        byte[] senderSignature = null;

        try {
            senderSignature = rsaServices.encrypt(masterWallet.getPublic_key(), dataHash.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        }
        result.setSenderSignature(senderSignature);

        return result;
    }

    @RequestMapping(value = "/loginUser", method = RequestMethod.POST)
    public ResponseEntity<UserRestWrapper> loginUser (@RequestBody User user){
        User loggedUser = userRepo.findOneByEmailAndPassword(user.getEmail(),user.getPassword());
        if (loggedUser== null ){
            return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        }

        if (user.getToken()!= null && !loggedUser.getToken().equals(user.getToken())){
            loggedUser.setToken(user.getToken());
            userRepo.save(loggedUser);
        }

        List<Wallet> wallets = walletRepo.findByWalletIdIn(loggedUser.getWallets());
        UserRestWrapper response = new UserRestWrapper(loggedUser, wallets);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

}
