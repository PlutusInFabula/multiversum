package com.multiversaworks.multichain.client.controllers.REST;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import lombok.Data;

import java.util.List;

@Data
public class WalletRestWrapper {
    private Wallet wallet;
    private WalletState walletState;
    private List<SimpleTransaction> pastTransactions;
}
