package com.multiversaworks.multichain.client.controllers.REST;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.multiversaworks.multichain.common.services.SimpleTransactionService;
import com.multiversaworks.multichain.common.services.WalletService;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.client.services.RestClient;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import com.universaworks.multiversa.multichain.common.services.SimpleTransactionService;
import com.universaworks.multiversa.multichain.common.services.WalletService;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class WalletRestController {

    //ToDO: make it distributed!

    @Autowired
    WalletStateService walletStateService;

    @Autowired
    WalletService walletService;

    @Autowired
    SimpleTransactionService simpleTransactionService;

    @Autowired
    RestClient client;

    @Autowired
    WalletRepo walletRepo;

    @RequestMapping(value = "/getrandomwallet", method = RequestMethod.GET)
    public ResponseEntity<Wallet> getRandomWallet(){
        Long qty = walletRepo.countAll();
        int idx = (int)(Math.random() * qty);
        Page<Wallet> questionPage = walletRepo.findAll(new PageRequest(idx, 1));
        Wallet q = null;
        if (questionPage.hasContent()) {
            q = questionPage.getContent().get(0);
        }
        return ResponseEntity.status(HttpStatus.OK).body(q);
    }

    @RequestMapping(value = "/getwallet/{wallet_id}", method = RequestMethod.GET)
    public ResponseEntity<WalletRestWrapper> getWallet(@PathVariable("wallet_id") String walletId){
        WalletRestWrapper response = new WalletRestWrapper();
        Wallet wallet = walletService.getWalletById(walletId);
        if (wallet==null){
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        response.setWallet(wallet);
        WalletState walletState = walletStateService.getLastStateofWallet(walletId);
        response.setWalletState(walletState);
        List<SimpleTransaction> pastTransactions = simpleTransactionService.getTransactionsForWalletId(walletId);
        response.setPastTransactions(pastTransactions);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/makeTransaction", method = RequestMethod.POST)
    public ResponseEntity makeTransaction(@RequestBody SimpleTransaction transaction){

        return client.postRequest(transaction);
    }

}
