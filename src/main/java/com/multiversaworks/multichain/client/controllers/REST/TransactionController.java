package com.multiversaworks.multichain.client.controllers.REST;

import com.multiversaworks.multichain.common.model.PageRequestWrapper;
import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.services.SimpleTransactionService;
import com.universaworks.multiversa.multichain.common.model.PageRequestWrapper;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.repositories.SimpleTransactionRepo;
import com.universaworks.multiversa.multichain.common.services.SimpleTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.data.domain.Pageable;

import java.util.List;

@RestController
public class TransactionController {

    @Autowired
    SimpleTransactionRepo simpleTransactionRepo;

    @Autowired
    SimpleTransactionService simpleTransactionService;

    @RequestMapping(value = "/getAllTransactions", method = RequestMethod.POST)
    public Page<String> getRunningTransactions(@RequestBody PageRequestWrapper wrapper){
        Pageable p = new PageRequest(wrapper.getPage(), wrapper.getLenght(), new Sort(Sort.Direction.DESC, "transactionFinished") );
        List <String> runningTransactions = simpleTransactionService.getRunningTransactions();
         return new PageImpl<String>(runningTransactions, p, runningTransactions.size() ) ;
    }

    @RequestMapping(value = "/getFinishedTransactions", method = RequestMethod.POST)
    public Page<SimpleTransaction> getFinishedTransactions(@RequestBody PageRequestWrapper wrapper){
        Pageable p = new PageRequest(wrapper.getPage(), wrapper.getLenght(), new Sort(Sort.Direction.DESC, "transactionFinished") );
        List<SimpleTransaction> allFinishedTransactions = simpleTransactionRepo.findAll();
        return new PageImpl<SimpleTransaction>(allFinishedTransactions, p, allFinishedTransactions.size() ) ;
    }

    @RequestMapping(value = "/getSingleTransactions", method = RequestMethod.POST)
    public SimpleTransaction getSingleTransactions(@RequestBody String trxId){
        SimpleTransaction result = simpleTransactionRepo.getOne(trxId);
        return result ;
    }

}