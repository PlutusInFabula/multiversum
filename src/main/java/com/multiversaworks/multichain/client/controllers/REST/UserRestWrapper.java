package com.multiversaworks.multichain.client.controllers.REST;

import com.multiversaworks.multichain.common.model.User;
import com.multiversaworks.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserRestWrapper {

    private User user;
    private List<Wallet> wallets;

}
