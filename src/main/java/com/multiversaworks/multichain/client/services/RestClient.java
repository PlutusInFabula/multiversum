package com.multiversaworks.multichain.client.services;

import com.multiversaworks.multichain.common.model.SimpleTransaction;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.common.cryptografy.RsaServices;
import com.universaworks.multiversa.multichain.common.model.SimpleTransaction;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.UserRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.UUID;

@Service
public class RestClient {

    @Value("${server.address}")
    String server;
    @Value("${server.port}")
    String port;

    @Autowired
    WalletStateService walletStateService;

    @Autowired
    WalletRepo walletRepo;

    @Autowired
    RsaServices rsaServices;

    public ResponseEntity postRequest(SimpleTransaction transaction){

        if (transaction.getVariation().floatValue()<=0) {
            return  ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Transaction amount should be bigger than Zero");
        }

        if (!transactionDataComplete( transaction)) {
            return  ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Missing data to complete the transaction");
        }

        if (transaction.getReceiverId().equals(transaction.getSenderId()) ){
            return  ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Can't send transaction to the same wallet");
        }

        // checks whether the hash of the data in transaction are he same as the user signed with own public key
        if (!calculateHash(transaction).equals( extractHash(transaction))) {
            return  ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Signature Not correct");
        }

        // Completes own part of data (Transaction Id and timestamp)
        transaction.setTransactionID(UUID.randomUUID().toString());
        transaction.setTransactionStart(LocalDateTime.now());

            // preventive control on wallet disponibility to save work
            WalletState ws  = walletStateService.getLastStateofWallet(transaction.getSenderId());
            if (ws.getActualValue().subtract( transaction.getVariation()).doubleValue() <= 0 ){
                return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).body("Transaction amount exceed wallet disponibility");
            }

        // ToDo: here is possible to check signature

        RestTemplate template = new RestTemplate();
        ResponseEntity o = template.postForEntity("http://"+server+":" + port + "/start-transaction/", transaction , String.class);
        return o;
    }

    private String extractHash(SimpleTransaction transaction) {
        Wallet actualWallet = walletRepo.findOneByWalletId(transaction.getSenderId());
        String result = null;
        try {
            result = rsaServices.decrypt(actualWallet.getPrivate_key(), transaction.getSenderSignature());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private String calculateHash(SimpleTransaction transaction) {
        StringBuilder sb = new StringBuilder();
        sb.append(transaction.getSenderId()).append(transaction.getReceiverId()).append(transaction.getVariation()).append(transaction.getText()).append(transaction.getTransactionStart());
        String result = DigestUtils.sha1Hex(sb.toString());
        return result;
    }

    private boolean transactionDataComplete(SimpleTransaction transaction) {
        
        if (walletRepo.findByWalletIdIn(Arrays.asList(transaction.getSenderId(),transaction.getReceiverId())).size() != 2) return false;

        if (transaction.getReceiverId() == null ||
            transaction.getSenderSignature()  == null ||
            transaction.getSenderId()== null ||
            transaction.getVariation() == null ||
            transaction.getSenderSignature() == null) return false;
        return true;
    }

}
