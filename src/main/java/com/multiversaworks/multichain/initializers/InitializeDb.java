package com.multiversaworks.multichain.initializers;

import com.multiversaworks.multichain.common.model.User;
import com.multiversaworks.multichain.common.model.Wallet;
import com.multiversaworks.multichain.common.model.WalletState;
import com.multiversaworks.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.common.cryptografy.RsaServices;
import com.universaworks.multiversa.multichain.common.model.User;
import com.universaworks.multiversa.multichain.common.model.Wallet;
import com.universaworks.multiversa.multichain.common.model.WalletState;
import com.universaworks.multiversa.multichain.common.repositories.UserRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletRepo;
import com.universaworks.multiversa.multichain.common.repositories.WalletStateRepo;
import com.universaworks.multiversa.multichain.common.services.WalletStateService;
import com.universaworks.multiversa.multichain.server.rest.StartTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.security.KeyPair;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;

@Component
public class InitializeDb {

    @Autowired
    WalletRepo walletRepo;
    @Autowired
    WalletStateRepo walletStateRepo;
    @Autowired
    WalletStateService walletStateService;
    @Autowired
    UserRepo userRepo;
    @Autowired
    StartTransaction startTransaction;
    @Autowired
    RsaServices rsaServices;

    @PostConstruct
    public void initializeWallet(){

        Wallet masterWallet = new Wallet();
        masterWallet.setWalletId("MasterWallet");

        KeyPair keyPair = rsaServices.generateKeyPair();
        masterWallet.setPrivate_key(keyPair.getPrivate().getEncoded());
        masterWallet.setPublic_key(keyPair.getPublic().getEncoded());

        WalletState masterWalletState = new WalletState();
        masterWalletState.setWalletId(masterWallet.getWalletId());
        masterWalletState.setActualValue(BigDecimal.valueOf(2147483647));
        masterWalletState.setPreviousHash("zero_hash");
        masterWalletState.setTimestamp(LocalDateTime.now());
        masterWalletState.setHash(masterWalletState.calculateOwnHash());

        User masterUser = new User();
        masterUser.setName("Master User");
        masterUser.setToken("");
        masterUser.setWallets(new ArrayList<>(Arrays.asList(masterWallet.getWalletId())));

        userRepo.save(masterUser);

        /*Wallet wallet_1 = new Wallet();
        wallet_1.setWalletId("w1");

        WalletState walletState_11 = new WalletState();
        walletState_11.setWalletId(wallet_1.getWalletId());
        walletState_11.setActualValue(BigDecimal.valueOf(0));
        walletState_11.setPreviousHash("zero_hash");
        walletState_11.setTimestamp(LocalDateTime.now());
        walletState_11.setHash(walletState_11.calculateOwnHash());

        Wallet wallet_2 = new Wallet();
        wallet_2.setWalletId("w2");

        WalletState walletState_21 = new WalletState();
        walletState_21.setWalletId(wallet_2.getWalletId());
        walletState_21.setActualValue(BigDecimal.valueOf(0));
        walletState_21.setPreviousHash("zero_hash");
        walletState_21.setTimestamp(LocalDateTime.now());
        walletState_21.setHash(walletState_21.calculateOwnHash());
*/
        walletRepo.save(masterWallet);
        //walletRepo.save(wallet_1);
        //walletRepo.save(wallet_2);
        walletStateRepo.save(masterWalletState);
        //walletStateRepo.save(walletState_11);
        //walletStateRepo.save(walletState_21);


     //String fake1 =   this.makeFakeAccount(new SimpleTransaction("MasterWallet", wallet_1.getWalletId(), BigDecimal.valueOf(10000), "No Signature yet."));
     //String fake2 = this.makeFakeAccount(new SimpleTransaction("MasterWallet", wallet_2.getWalletId(), BigDecimal.valueOf(11000), "No Signature yet."));

    }

   /* private String makeFakeAccount(SimpleTransaction transaction){
        transaction.setTransactionID(UUID.randomUUID().toString());
        transaction.setTransactionStart(LocalDateTime.now());
        WalletState ws  = walletStateService.getLastStateofWallet(transaction.getSenderId());
        if (ws.getActualValue().subtract( transaction.getVariation()).doubleValue() <= 0 ){
            throw new IllegalArgumentException("Transaction amount exceed wallet disponibility");
        }
        String response = startTransaction.startTransaction(transaction).getBody();
        return response;
    }*/

}
