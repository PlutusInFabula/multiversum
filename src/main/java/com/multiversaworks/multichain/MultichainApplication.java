package com.multiversaworks.multichain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class MultichainApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultichainApplication.class, args);
	}
}
