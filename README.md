# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is a new protocol for distributed data chains, called Multiversa.
The main scope of Multiversa is achieving a self susteanable cluster of servers, that autoelect their rules upon technical parameters, 
and create an internal coordination circle, a messenger helper circle and an external circle of work executors.

Multiversa achieve horizontal scalability forking the chains upon intensive load and rejoining them as soon as work load diminuish.

Multiversa use a time ordered (FIFO) list of blocked hashes to maintain chronological coherence.

![Alt text](https://bytebucket.org/nuspy/multiversum/raw/fa9bf28afe40993fd0d8d9eb9c22adaa725a1ab7/Activity%20Diagram%20Multiversa.jpg?raw=true "Activity Diagram")


### How do I get set up? ###

The main idea is to distribute two Dockerized containers, one containing the whole system (server, messengers, workers, and simple single wallet)
One containing only the simple single wallet manager

A further layer of security iw sarranteed by Docker itself that won't allow to change components inside.

### License ###

While the code is Open Source to allow security audit, the whole work is protected by MIT License term.
Copy Right 25.XII.2017